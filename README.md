# My vim config file

My custom vim configuration.

Requires ```Plug``` to install plugins
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Plugins requires some additional packages: ```cmake python ctags clang ncurses-compat-libs```

Use this command when you start first time:
```vim +PlugInstall```
