set nocompatible              " be iMproved, required

"Fix Alt+[key] for some terminals
let c='a'
while c <= 'z'
    exec "set <A-".c.">=\e".c
    exec "imap \e".c." <A-".c.">"
    let c = nr2char(1+char2nr(c))
endw

let c='1'
while c <= '9'
    exec "set <A-".c.">=\e".c
    exec "imap \e".c." <A-".c.">"
    let c = nr2char(1+char2nr(c))
endw

set timeout ttimeoutlen=50

" Plugin manager
call plug#begin('~/.vim/plugged')

"---------=== Code/project navigation ===-------------
Plug 'scrooloose/nerdtree' 	    	" Project and file navigation

"--------------===Autocomplete===---------------
Plug 'Valloric/YouCompleteMe', { 'do' : './install.py --clang-completer' }
" Plug 'rdnetto/YCM-Generator'
Plug 'https://github.com/rdnetto/YCM-Generator.git'


" highlight extra whitespaces
Plug 'ntpeters/vim-better-whitespace'

Plug 'vim-scripts/taglist.vim'

Plug 'https://github.com/vim-scripts/indentpython.vim.git', { 'for': 'python' }

" Syntax checking
Plug 'vim-syntastic/syntastic'

call plug#end()            		" required

filetype on
filetype plugin on
filetype plugin indent on


"=====================================================
"" General settings
"=====================================================
syntax on
highlight NonText ctermfg=white

"highlight column with 80 character
set colorcolumn=80

set fillchars+=vert:\|
hi vertsplit guifg=fg guibg=bg
let mapleader = "\<Space>"

"Fix backspace bug
set backspace=indent,eol,start
aunmenu Help.
aunmenu Window.

"mouse support
set mouse=a

set ruler

"tabulation options
set tabstop=4
set softtabstop=4
set shiftwidth=4

set expandtab
set ai
set autoindent
set smartindent
set cino+=(0

"Always show statusbar at the bottom
set laststatus=2

set showmatch
set number

set visualbell t_vb=
set novisualbell

set enc=utf-8
set hid
set ignorecase
set incsearch
set hlsearch

set magic
set mat=2

"bash-like completion in vim commands
set wildmode=longest,list

"Character border color (colorcolumn)
hi ColorColumn ctermbg=0

"highlight same words under cursor
:autocmd CursorMoved * exe printf('match IncSearch /\V\<%s\>/', escape(expand('<cword>'), '/\'))

"let Tlist_Ctags_Cmd=~/.vimrc/plugin/taglist.vim
let Tlist_Inc_Winwidth=0
let Tlist_Use_Right_Window=1
let Tlist_Auto_Update=1
let Tlist_Compact_Format=1
let Tlist_Enable_Fold_Column=0
let Tlist_Display_Tag_Scope=0
let g:Tlist_GainFocus_On_ToggleOpen = 1
autocmd BufWritePost *.cpp :TlistUpdate

let NERDTreeShowHidden=1
let NERDTreeStatusLine=0
let NERDTreeMinimalUI=1
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$']

""""""""""YCM""""""""""
let g:ycm_confirm_extra_conf = 0
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_echo_current_diagnostic = 1

map <M-s> :tab split<CR>
map <M-d> :YcmCompleter GoToDeclaration<CR>
map <M-f> :YcmCompleter GoToDefinition<CR>
map <M-g> :YcmCompleter GoTo<CR>
map <M-y> :YcmCompleter 


"Show buffer list and select buffer number to open
map <M-l> :ls<CR>:b<Space>
"vertical split + open selected buffer
map <M-v> :vs<CR> :wincmd l<CR> :ls<CR>:b<Space>

"Save bindings
nmap <F5> :w<CR>
nmap <F6> :wa<CR>
imap <F5> <ESC>:w<CR>i
imap <F6> <ESC>:wa<CR>i

"close all without save
nmap <S-Q> :qa!<CR>
"close all whith save
nmap <S-W> :xa<CR>
map <F9> :make<CR>
map <S-F9> :make clean all<CR>

nnoremap <silent> <F3> :TlistToggle<CR>
nnoremap <silent> <F2> :NERDTreeToggle<CR>

nnoremap <C-y> "+y
vnoremap <C-y> "+y
nnoremap <C-p> "+gP
vnoremap <C-p> "+gP

vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P
nmap <Leader><Leader> V

"Tab navigation
nnoremap <silent> <M-h> :tabp<CR>
nnoremap <silent> <M-l> :tabn<CR>
nnoremap <silent> <M-k> :tabp<CR>
nnoremap <silent> <M-j> :tabn<CR>
"Window navigation
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>

map <M-t> :tabnew<CR>
map <M-1> 1gt
map <M-2> 2gt
map <M-3> 3gt
map <M-4> 4gt
map <M-5> 5gt
map <M-6> 6gt
map <M-7> 7gt
map <M-8> 8gt
map <M-9> 9gt
